package com.ventaja.calorie.alarm;

import android.app.ActionBar.Tab;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;

class MyTabsListener implements ActionBar.TabListener {
	public Fragment fragment;
	 
	public MyTabsListener(Fragment fragment) {
		this.fragment = fragment;
	}
	 
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}
	 
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		ft.replace(R.id.fragment_container, fragment);
	}
	 
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		ft.remove(fragment);
	}
}