package com.ventaja.calorie.alarm;


import android.R.color;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class Tab_History extends Fragment {
	final int PLOT_LENGTH = 7;
	private int selectedEntry = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	//// Create the view first so it can be used
    	final View view = inflater.inflate(R.layout.tab_history, container, false);    	
   		LinearLayout historyList = (LinearLayout) view.findViewById(R.id.historyListLayout);

   		
   		//// Add history list
   		// Get the history from storage
   		final History history = new History(getActivity());
       	final String[] historyArray = history.getHistoryList();
       	
       	// Display the history if there is one
       	if (historyArray != null) {

       		// Create the edit text used in the alert dialog
       		final EditText alertEditText = new EditText(getActivity());
	    		
       		// Create the listener for updating the daily calorie counts
       		android.content.DialogInterface.OnClickListener updateCalorieCountListener = new android.content.DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					Integer newCalorieCount = -100000;
					
					try {
						newCalorieCount = Integer.parseInt(alertEditText.getText().toString());
					} catch (Exception e) {
						// TODO
					}
					
					
					if ((alertEditText.length() > 0) && (selectedEntry > 0) && newCalorieCount != -100000) {
						// Save the new count to the file
						String date = historyArray[selectedEntry-1].split(",")[0] + "," + historyArray[selectedEntry-1].split(",")[1] + "," + historyArray[selectedEntry-1].split(",")[2];
						historyArray[selectedEntry-1] = date + "," + newCalorieCount.toString();
						history.writeNewHistory(historyArray);
									
						// Update the displayed text
						getActivity().recreate();
					}
				}
       		};
       		
	    	// Create the editing screen
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(R.string.updateCalorieCount)
			       .setCancelable(true)
			       .setPositiveButton("Update", updateCalorieCountListener)
			       .setNegativeButton("Cancel", null)
			       .setView(alertEditText);
			final AlertDialog dialog = builder.create();
			
			// Populate the calorie entries
   	    	for(int i = historyArray.length - 1; i >= 0 ; i--) {
   	    		// Create the date text
   	    		final TextView dateEntry = new TextView(getActivity());
   	    		dateEntry.setText(historyArray[i].split(",")[2] + "-" + historyArray[i].split(",")[1] + "-" + historyArray[i].split(",")[0]);
   	    		dateEntry.setTextSize(17);
   	    		
   	    		// Create the calorie count text
   	    		final TextView calorieEntry = new TextView(getActivity());
   	    		calorieEntry.setText(historyArray[i].split(",")[3]);
   	    		calorieEntry.setTextSize(17);
   	    		calorieEntry.setWidth(110);
   	    		
   	    		// Create the cell that holds the textviews
   	    		final RelativeLayout horizontalBar = new RelativeLayout(getActivity());
   	    		horizontalBar.setPadding(5, 5, 5, 5);
   	    		horizontalBar.setClickable(true);
   	    		horizontalBar.setFocusable(true);
   	    		horizontalBar.setFocusableInTouchMode(true);
   	    		horizontalBar.setId(i+1);
   	    		
   	    		RelativeLayout.LayoutParams relativeParams1 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
   	    		relativeParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
   	    		horizontalBar.addView(dateEntry, relativeParams1);
   	    		
   	    		RelativeLayout.LayoutParams relativeParams2 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
   	    		relativeParams2.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
   	    		horizontalBar.addView(calorieEntry, relativeParams2);
   	    		
   	    		// Add the cells to the screen
   	    		historyList.addView(horizontalBar);
   	    		
   	    		// Update row color depending on focus
   	    		horizontalBar.setOnFocusChangeListener(new OnFocusChangeListener() {
					public void onFocusChange(View arg0, boolean hasFocus) {
						if (hasFocus) {
							horizontalBar.setBackgroundResource(color.holo_blue_dark);
						} else {
							horizontalBar.setBackgroundResource(color.transparent);
						}
					}
   	    			
   	    		});
   	    		
   	    		
   	    		

   	    		// Bring up editing screen when a row is pressed
   	    		horizontalBar.setOnClickListener(new OnClickListener() {
					public void onClick(View arg0) {
						// Set the initial calorie count in the edit box
						selectedEntry = horizontalBar.getId();
						alertEditText.setText(calorieEntry.getText());
						alertEditText.setSelection(alertEditText.getText().length());
						alertEditText.setRawInputType(Configuration.KEYBOARD_12KEY);
						dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
						dialog.show();
					}
   	    		});
   	    	}
       	}
   		
   		return view;
    }
}