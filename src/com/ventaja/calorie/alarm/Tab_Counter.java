package com.ventaja.calorie.alarm;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class Tab_Counter extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//// Create the view for this fragment. Needs to be done here so widgets can be accessed in this method
		View view = inflater.inflate(R.layout.tab_counter, container, false);


		//// Get stored calorie count from persistent storage and display it
		SharedPreferences settings = getActivity().getSharedPreferences("CalorieAlarmPrefs", 0);   	 
		TextView calorieCountLabel = (TextView) view.findViewById(R.id.calorieCountLabel);
		calorieCountLabel.setText(String.valueOf(settings.getInt("CalorieCount", 0)));

		
		
		
		
		
		
		
		
		
		
		
		
		

   		final EditText alertEditText = new EditText(getActivity());
		
		// Create the listener for updating the daily calorie counts
   		android.content.DialogInterface.OnClickListener updateCalorieCountListener = new android.content.DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				int calorieValue = 0;
				try {
					calorieValue = Integer.parseInt(alertEditText.getText().toString());
				} catch (Exception e) {
					// TODO
				}
				
				// Get the current calorie count from persistent storage
				SharedPreferences settings = getActivity().getSharedPreferences("CalorieAlarmPrefs", 0);
				int initialCalories = settings.getInt("CalorieCount", 0);
				
				// Set the new calorie count
				int newCalories = initialCalories + calorieValue;
				// Only if the number is not too absurd
				if (newCalories > -20000 && newCalories < 20000) {
					// Save the new calorie count to persistent storage if it is not too large
					SharedPreferences.Editor editor = settings.edit();
					editor.putInt("CalorieCount", newCalories);
					editor.apply();
					// Display the new calorie count
					TextView calorieCountLabel = (TextView) getActivity().findViewById(R.id.calorieCountLabel);
					calorieCountLabel.setText(String.valueOf(newCalories));
					
					// Update the displayed text
					getActivity().recreate();
				}
			}
   		};
   		
   		
    	// Create the editing screen
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.updateCalorieCount)
		       .setCancelable(true)
		       .setPositiveButton("Add", updateCalorieCountListener)
		       .setNegativeButton("Cancel", null)
		       .setView(alertEditText);
		final AlertDialog dialog = builder.create();
		
		
		


		

		// Setup update calorie count button
		Button updateCaloriesButton = (Button) view.findViewById(R.id.updateCountButton);
		updateCaloriesButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				alertEditText.setSelection(alertEditText.getText().length());
				alertEditText.setRawInputType(Configuration.KEYBOARD_12KEY);
				dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
				dialog.show();
			}
		});
		
		
		
		
		

		/*

		Button SEEHIST = (Button) view.findViewById(R.id.SEEHIST);
		SEEHIST.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				History history = new History(getActivity());
				// Show the history
				Log.w("LOG", history.getHistory());
			}
		});
		
		
		

		Button ADDHIST = (Button) view.findViewById(R.id.ADDHIST);
		ADDHIST.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				History history = new History(getActivity());
				
				// Add History
				Calendar cal = Calendar.getInstance();
				//cal.add(Calendar.DATE, -10);
				//history.clear();
				history.addEntry(cal, 1000);
			}
		});
*/
        return view;
    }
}