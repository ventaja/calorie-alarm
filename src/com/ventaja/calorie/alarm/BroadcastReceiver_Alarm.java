package com.ventaja.calorie.alarm;

import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.widget.Toast;

public class BroadcastReceiver_Alarm extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {

		Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		Ringtone ringer = RingtoneManager.getRingtone(context, notification);
		Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		
		switch (audioManager.getRingerMode()) {
		    case AudioManager.RINGER_MODE_SILENT:
				// Create a message to the user
				Toast.makeText(context, "Time to update calorie count", Toast.LENGTH_LONG).show();
		        break;
		    case AudioManager.RINGER_MODE_VIBRATE:
				// Create a message to the user
				Toast.makeText(context, "Time to update calorie count", Toast.LENGTH_LONG).show();
				// Vibrate the mobile phone
				vibrator.vibrate(3000);
		        break;
		    case AudioManager.RINGER_MODE_NORMAL:
				// Create a message to the user
				Toast.makeText(context, "Time to update calorie count", Toast.LENGTH_LONG).show();
				// Vibrate the mobile phone
				vibrator.vibrate(3000);
				// Make a sound
				ringer.play();
		        break;
		}
		
		
		
		// Bring up calorie alarm counter page
		Intent openCalorieAlarmActivity = new Intent(context, Activity_Main.class);
	    openCalorieAlarmActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
	    context.startActivity(openCalorieAlarmActivity);    
	}
} 