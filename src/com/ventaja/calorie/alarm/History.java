package com.ventaja.calorie.alarm;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;

import android.content.Context;

public class History {
	final String LOG_FILE = "count_history";
	Context context;
	
	History(Context context) {
		this.context = context;
	}
	 
	

    
	
	
	String[] getHistoryList() {
	    String[] historyList = null;
		try {
		    BufferedReader inputReader = new BufferedReader(new InputStreamReader(context.openFileInput(LOG_FILE)));
		    String inputString;
		    if ((inputString = inputReader.readLine()) != null) {
		    	historyList = inputString.split(";");
		    }
		} catch (IOException e) {
		    e.printStackTrace();
		}
		return historyList;
	}
	 
	

    
	
	
	String getHistory() {
	    String history = null;
		try {
		    BufferedReader inputReader = new BufferedReader(new InputStreamReader(context.openFileInput(LOG_FILE)));
		    history = inputReader.readLine();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		return history;
	}
	
	
	

	
	
	
	
	void writeNewHistory(String[] values) {
		FileOutputStream fos;
		try {
			fos = context.openFileOutput(LOG_FILE, Context.MODE_PRIVATE);
			for(int i = 0; i < values.length; i++) {
				fos.write((values[i].toString() + ";").getBytes());
			}
			fos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	
	public void addEntry(Calendar cal, int calorieCount) {
	
		// Make sure the entry is newer than the newest entry in the history list
		String[] historyList = getHistoryList();
		if (historyList != null && historyList.length > 0) {
			String[] lastEntry = historyList[historyList.length-1].split(",");
			Calendar lastEntryCal = Calendar.getInstance();
			lastEntryCal.set(Integer.parseInt(lastEntry[0]), Integer.parseInt(lastEntry[1]), Integer.parseInt(lastEntry[2]));
			if  (lastEntryCal.before(cal)) {
			
				// Find the entries that must be added before the given one and add them
				FileOutputStream fos;
				try {
					fos = context.openFileOutput(LOG_FILE, Context.MODE_APPEND);
					lastEntryCal.add(Calendar.DATE, 1);
					
					while (lastEntryCal.before(cal)) {
						fos.write((
								String.valueOf(lastEntryCal.get(Calendar.YEAR)) 			+ "," + 
								String.valueOf(lastEntryCal.get(Calendar.MONTH))			+ "," +
								String.valueOf(lastEntryCal.get(Calendar.DAY_OF_MONTH)) 	+ ",0;"
							).getBytes());
						
						lastEntryCal.add(Calendar.DATE, 1);
					}
					
					fos.write((
							String.valueOf(cal.get(Calendar.YEAR)) 			+ "," + 
							String.valueOf(cal.get(Calendar.MONTH))			+ "," +
							String.valueOf(cal.get(Calendar.DAY_OF_MONTH)) 	+ "," + 
							String.valueOf(calorieCount) 					+ ";"
						).getBytes());
					fos.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
		} else {
			// Add the first entry
			FileOutputStream fos;
			try {
				fos = context.openFileOutput(LOG_FILE, Context.MODE_APPEND);
				fos.write((
						String.valueOf(cal.get(Calendar.YEAR)) 			+ "," + 
						String.valueOf(cal.get(Calendar.MONTH))			+ "," +
						String.valueOf(cal.get(Calendar.DAY_OF_MONTH)) 	+ "," + 
						String.valueOf(calorieCount) 					+ ";"
					).getBytes());
				fos.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	
	
	
	public void clear() {
		FileOutputStream fos;
		try {
			fos = context.openFileOutput(LOG_FILE, Context.MODE_PRIVATE);
			fos.write(("").getBytes());
			fos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
