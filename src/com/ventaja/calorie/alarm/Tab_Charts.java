package com.ventaja.calorie.alarm;

import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;

import android.R.color;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class Tab_Charts extends Fragment {
	final String LOG_FILE = "count_history";
	int PLOT_LENGTH = 7;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.tab_charts, container, false);
    	
    	final LineGraphView graphView = new LineGraphView(getActivity(), "");

    	final History history = new History(getActivity());
    	
    	String[] calorieLog = history.getHistoryList();

    	
    	if (calorieLog != null && calorieLog.length > 0) {
    		
	    	//if (true || calorieLog.length < PLOT_LENGTH) {
    		// TODO 
	    		PLOT_LENGTH = calorieLog.length;
	    	//}
    	
    		GraphViewData[] graphData = new GraphViewData[PLOT_LENGTH];
    		String[] days = new String[PLOT_LENGTH];
    		
    		// If the number of log entries are greater than the number of points to show, then shift index to show latest entries
    		int startIndex = 0;
    		if (calorieLog.length > PLOT_LENGTH) {
    			startIndex = calorieLog.length - PLOT_LENGTH;
    		}
    		
    		// Add all the log entries
    		for (int i = startIndex; i < calorieLog.length; i++) {
    			graphData[i - startIndex] = new GraphViewData(i, Double.parseDouble(calorieLog[i].split(",")[3]));
    			days[i - startIndex] = new String(calorieLog[i].split(",")[2]);
    		}
        	
    		// Show the plot
    		//graphView.setViewPort(0, 7);
    		graphView.setHorizontalScrollBarEnabled(true);
    		graphView.setScrollable(true);
    		graphView.setBackgroundColor(color.holo_blue_dark);
    		graphView.setDrawBackground(true);
    		graphView.addSeries(new GraphViewSeries(graphData));
    		graphView.setHorizontalLabels(days);
    		
    		LinearLayout layout = (LinearLayout) view.findViewById(R.id.chartsGraph);
        	layout.addView(graphView);
        	
    	/*
			//// Set up the button used to select day view
			Button setDays = (Button) view.findViewById(R.id.daysButton);
			setDays.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					
				}
			});
	    	
	
			//// Set up the button used to select week view
			Button setWeeks = (Button) view.findViewById(R.id.weeksButton);
			setWeeks.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					
				}
			});
	    	
	
			//// Set up the button used to select month view
			Button setMonths = (Button) view.findViewById(R.id.monthsButton);
			setMonths.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					
				}
			});
		*/
    	} else {
    		
    	}
    	
        return view;
    }
}