package com.ventaja.calorie.alarm;

import java.util.Calendar;

import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.widget.ArrayAdapter;

public class Activity_Main extends FragmentActivity implements OnNavigationListener {
    private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
    private Fragment counterFragment;
    private Fragment logFragment;
    private Fragment historyFragment;
    private Fragment alarmsFragment;
    ActionBar actionbar;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //// Setup ActionBar
        // Set up action bar spinner
        ArrayAdapter<CharSequence> list = ArrayAdapter.createFromResource(this, R.array.fragment_array, android.R.layout.simple_spinner_item);
        list.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
        
        // ActionBar gets initiated
        actionbar = getActionBar();
        actionbar.setDisplayShowTitleEnabled(false);
        actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionbar.setListNavigationCallbacks(list, this);
        
        
        
        /*
        // initiating tabs and set text to it.
        ActionBar.Tab counterTab = actionbar.newTab().setText(R.string.nav_counter);
        ActionBar.Tab logTab = actionbar.newTab().setText(R.string.nav_charts);
        ActionBar.Tab historyTab = actionbar.newTab().setText(R.string.nav_history);
        ActionBar.Tab alarmsTab = actionbar.newTab().setText(R.string.nav_alarms);
        */
        // create the fragments we want to use for display content
        counterFragment = new Tab_Counter();
        logFragment = new Tab_Charts();
        historyFragment = new Tab_History();
        alarmsFragment = new Tab_Alarms();
 
        // Show the counter fragment by default
		FragmentManager fragmentManager = getFragmentManager();
		android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.add(R.id.fragment_container, counterFragment);
		fragmentTransaction.commit();
        
        /*
        // set the Tab listener. Now we can listen for clicks.
        counterTab.setTabListener(new MyTabsListener(counterFragment));
        logTab.setTabListener(new MyTabsListener(logFragment));
        historyTab.setTabListener(new MyTabsListener(historyFragment));
        alarmsTab.setTabListener(new MyTabsListener(alarmsFragment));
 
        // add the tabs to the action bar
        actionbar.addTab(counterTab);
        actionbar.addTab(logTab);
        actionbar.addTab(historyTab);
        actionbar.addTab(alarmsTab);
        */
        
        
        //// create an alarm to update log, and calorie count at the start of each day
		AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		//This is the intent that is launched when the alarm goes off.
        Intent intent = new Intent(this, BroadcastReceiver_UpdateLog.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
		// Make sure the alarm goes off at the end of the day
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND,59);
		calendar.set(Calendar.MILLISECOND, 999);
		am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }
	
	
	
	 @Override
	 protected void onNewIntent(Intent intent) {
		 /*
		FragmentManager fragmentManager = getFragmentManager();
		android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.fragment_container, counterFragment);
		fragmentTransaction.commit();
		*/
	     super.onNewIntent(intent);
	 }




    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
            getActionBar().setSelectedNavigationItem(
                    savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_SELECTED_NAVIGATION_ITEM,
                getActionBar().getSelectedNavigationIndex());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_counter, menu);
        return true;
    }



    // Handle action bar spinner  selections
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		FragmentManager fragmentManager = getFragmentManager();
		android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		
		if (itemPosition == 0) {
			fragmentTransaction.replace(R.id.fragment_container, counterFragment);
		} else if (itemPosition == 1) {
			fragmentTransaction.replace(R.id.fragment_container, logFragment);
		} else if (itemPosition == 2) {
			fragmentTransaction.replace(R.id.fragment_container, historyFragment);
		} else if (itemPosition == 3) {
			fragmentTransaction.replace(R.id.fragment_container, alarmsFragment);
		}
		
		fragmentTransaction.commit();
		
		return true;
	}
}