package com.ventaja.calorie.alarm;

import java.util.Calendar;

import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.SharedPreferences;

public class BroadcastReceiver_UpdateLog extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {		
		
		// Get calorie count
		SharedPreferences settings = context.getSharedPreferences("CalorieAlarmPrefs", 0);
		int calorieCount = settings.getInt("CalorieCount", 0);
		
		
		// Store calorie count in history file
		History history = new History(context);
		history.addEntry(Calendar.getInstance(), calorieCount);
		

		// Reset calorie count
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("CalorieCount", 0);
		editor.apply();
	}
} 